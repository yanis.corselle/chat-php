<?php
session_start();
//si l'utilisateur est connecté, il est redirigé vers la page des messages
if(isset($_SESSION['id']))
{
    header('Location: messages.php');
    exit();
}